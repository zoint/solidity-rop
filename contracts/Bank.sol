// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

import "./CustomerCore.sol";
import "./Borrower.sol";
import "./Investor.sol";

contract Bank {

    Customer internal arbli;

    constructor() payable {
        arbli = new CustomerCore("Arbli Troshani");
    }

    function makeTransactions() external {
        arbli.deposit(50000);
        arbli.withdraw(10000);
        // balance should be 40000 at this point

        Borrower borrower = new Borrower();
        arbli.addRole(borrower);
        borrower.borrow(2000);

        Investor investor = new Investor();
        arbli.addRole(investor);
        investor.invest(5000);
        // balance should be 35000 at this point

        investor.withdrawInvestment(2000);
        // balance should be 37000 at this point

        arbli.removeRole(borrower);
    }

    function investMonthlyAmount() external {
        uint investorRole = CustomerRoleTypes.INVESTOR;
        require(arbli.hasRoleType(investorRole), "Customer is not an investor.");

        address investorRoleAddress = arbli.getRole(investorRole);
        Investor investor = Investor(investorRoleAddress);
        investor.invest(5000);
        //balance should be 32000 at this point (if called sequentially)

        arbli.removeRole(investor);
    }

    function getCustomerBalance() external view returns (uint256)  {
        return arbli.currentBalance();
    }
}
