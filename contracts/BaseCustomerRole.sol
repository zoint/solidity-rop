// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

import "./Customer.sol";

abstract contract BaseCustomerRole is CustomerRole {

    Customer internal core;

    function addCore(Customer _core) external override {
        core = _core;
    }

    function roleType() external pure virtual override returns (uint);

    function hasRoleType(uint _roleType) external view override returns (bool) {
        return core.hasRoleType(_roleType);
    }

    function getRole(uint _roleType) external view override returns (address) {
        return core.getRole(_roleType);
    }

    function addRole(CustomerRole role) external override {
        core.addRole(role);
    }

    function removeRole(CustomerRole role) external override {
        core.removeRole(role);
    }

    function identifier() external view override returns (string memory) {
        return core.identifier();
    }

    function currentBalance() external view override returns (uint256) {
        return core.currentBalance();
    }

    function deposit(uint256 amount) external override {
        core.deposit(amount);
    }
    
    function withdraw(uint256 amount) external override {
        core.withdraw(amount);
    }
}
