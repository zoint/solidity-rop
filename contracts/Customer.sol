// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

library CustomerRoleTypes {
    uint constant BORROWER = 0;
    uint constant INVESTOR = 1;
}

interface Customer {
    // role-related functions
    function hasRoleType(uint roleType) external view returns (bool);
    function getRole(uint roleType) external view returns (address);
    function addRole(CustomerRole role) external;
    function removeRole(CustomerRole role) external;

    // customer-related functions
    function identifier() external view returns (string memory);
    function currentBalance() external view returns (uint256);
    function deposit(uint256 amount) external;
    function withdraw(uint256 amount) external;
}

interface CustomerRole is Customer {
    function roleType() external pure returns (uint);
    function addCore(Customer core) external;
}
