// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

import "./BaseCustomerRole.sol";

contract Investor is BaseCustomerRole {

    uint256 public investAmount;

    function roleType() public pure override returns (uint) {
        return CustomerRoleTypes.INVESTOR;
    }

    function invest(uint256 amount) public {
        require(amount <= core.currentBalance(), "Insufficient funds to invest");
        core.withdraw(amount);
        investAmount += amount;
    }

    function withdrawInvestment(uint256 amount) public {
        require(amount <= investAmount, "Insufficient invested funds.");
        investAmount -= amount;
        core.deposit(amount);
    }
}
