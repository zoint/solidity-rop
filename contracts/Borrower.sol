// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

import "./BaseCustomerRole.sol";

contract Borrower is BaseCustomerRole {

    uint256 public borrowAmount;

    function roleType() public pure override returns (uint) {
        return CustomerRoleTypes.BORROWER;
    }

    function borrow(uint256 amount) public {
        borrowAmount += amount;
    }

    function payBack(uint256 amount) public {
        borrowAmount -= amount;
    }
}
