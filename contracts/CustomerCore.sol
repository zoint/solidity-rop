// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

import "./Customer.sol";

contract CustomerCore is Customer {

    string internal name;

    uint256 internal balance;

    mapping(uint => address) internal activeRoles;

    constructor(string memory _name) {
        name = _name;
    }

    function hasRoleType(uint _roleType) external view override returns (bool) {
        return activeRoles[_roleType] != address(0);
    }

    function getRole(uint _roleType) external view override returns (address) {
        require(this.hasRoleType(_roleType), "Requested RoleType is not being played.");
        return activeRoles[_roleType];
    }

    function addRole(CustomerRole role) external override {
        uint _roleType = role.roleType();
        activeRoles[_roleType] = address(role);
        role.addCore(this);
    }

    function removeRole(CustomerRole role) external override {
        uint _roleType = role.roleType();
        activeRoles[_roleType] = address(0);
    }

    function identifier() external view override returns (string memory) {
        return string(abi.encodePacked("name:", name));
    }

    function currentBalance() external view override returns (uint256) {
        return balance;
    }

    function deposit(uint256 amount) external override {
        balance += amount;
    }
    
    function withdraw(uint256 amount) external override {
        require(amount <= balance, "Insufficient funds");
        balance -= amount;
    }
}
